import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private final Socket socket;
    private int id;
    private int idEmisor = 0;
    private boolean isChat;

    public int getIdEmisor() {
        return idEmisor;
    }

    public void setChat(boolean chat) {
        isChat = chat;
    }

    public Client(Socket socket) {
        this.socket = socket;
        isChat = false;
        crearId();
        System.out.println("Cliente añadido: " + id);
    }

    private void crearId() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);
            id = Integer.parseInt(br.readLine());
            pw.println("OK");
            idEmisor = Integer.parseInt(br.readLine());
            System.out.println("Id emisor = " + idEmisor);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public Socket getSocket() {
        return socket;
    }

    public boolean isInChat() {
        return isChat;
    }
}
