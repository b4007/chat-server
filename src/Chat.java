import java.io.IOException;
import java.io.PrintWriter;

public class Chat {
    Client client1;
    Client client2;

    public Chat(Client client1, Client client2) {
        this.client1 = client1;
        this.client2 = client2;
        client1.setChat(true);
        client2.setChat(true);
    }

    public Client getClient1() {
        return client1;
    }

    public Client getClient2() {
        return client2;
    }
}
