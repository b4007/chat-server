import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;

public class Server {
    static Socket socket = null;
    static ArrayBlockingQueue<Client> clients = new ArrayBlockingQueue<>(2048);

    public synchronized static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(9090);
        socket = serverSocket.accept();
        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        clients.add(new Client(socket));
        ArrayBlockingQueue<Chat> chats = new ArrayBlockingQueue<Chat>(1024);


        Thread acceptAllClients = new Thread(new Runnable() {
            @Override
            public void run() {
                do {
                    try {
                        socket = serverSocket.accept();
                        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        clients.add(new Client(socket));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } while (true);
            }
        });

        Thread addChat = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    for (Client client : clients) {
                        if (!client.isInChat() && getClientById(client) != null) {
                            chats.add(new Chat(client, getClientById(client)));
                        }
                    }
                }
            }
        });

        Thread redirectToClient2 = new Thread(new Runnable() {
            @Override
            public void run() {
                PrintWriter pw;
                BufferedReader br;

                PrintWriter pw1;
                BufferedReader br1;
                while (true) {
                    if (!chats.isEmpty()) {
                        for (Chat chat : chats) {
                            if (chat != null){
                                try {
                                    if (chat.client1.isInChat() && chat.client2.isInChat()) {
                                        System.out.println(chat.client1.getId() + " is connected to " + chat.client2.getId());
                                        pw = new PrintWriter(chat.client1.getSocket().getOutputStream(), true);
//                                        br = new BufferedReader(new InputStreamReader(chat.client1.getSocket().getInputStream()));

//                                        pw1 = new PrintWriter(chat.client2.getSocket().getOutputStream(), true);
                                        br1 = new BufferedReader(new InputStreamReader(chat.client2.getSocket().getInputStream()));

//                                        pw1.println(br.readLine());
                                        String line = "";
                                        if ((line = br1.readLine()) != null){
                                            pw.println(line);
                                        }
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        });

        Thread redirectToClient1 = new Thread(new Runnable() {
            @Override
            public void run() {
                PrintWriter pw;
                BufferedReader br;

                PrintWriter pw1;
                BufferedReader br1;
                while (true) {
                    if (!chats.isEmpty()) {
                        for (Chat chat : chats) {
                            if (chat != null){
                                try {
                                    if (chat.client1.isInChat() && chat.client2.isInChat()) {
                                        System.out.println(chat.client1.getId() + " is connected to " + chat.client2.getId());
//                                    pw = new PrintWriter(chat.client1.getSocket().getOutputStream(), true);
                                        br = new BufferedReader(new InputStreamReader(chat.client1.getSocket().getInputStream()));

                                        pw1 = new PrintWriter(chat.client2.getSocket().getOutputStream(), true);
//                                    br1 = new BufferedReader(new InputStreamReader(chat.client2.getSocket().getInputStream()));
                                        String line = "";
                                        if ((line = br.readLine()) != null){
                                            pw1.println(line);
                                        }
//                                    pw.println(br1.readLine());
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        });

        Thread clientOutOfChat = new Thread(new Runnable() {
            @Override
            public void run() {
             while (true) {
                 if (!chats.isEmpty()){
                     for (Chat chat : chats) {
                         if (chat.client1.getSocket().isClosed()){
                             chat.client1.setChat(false);
                         }

                         if (chat.client2.getSocket().isClosed()){
                             chat.client2.setChat(false);
                         }
                     }
                 }
             }
            }
        });

        acceptAllClients.start();
        addChat.start();
        redirectToClient2.start();
        redirectToClient1.start();
        clientOutOfChat.start();
    }

    protected static Client getClientById(Client client1) {
        for (Client client : clients) {
            if (client.getId() == client1.getIdEmisor()) {
                return client;
            }
        }
        return null;
    }
}

